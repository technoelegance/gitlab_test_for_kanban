## v0.0.2 (13.07.2016 00:33) 

### Changes 
* bump version ([#2](https://gitlab.com/technoelegance/gitlab_test_for_kanban/merge_requests/2) by [@gkopylov](https://gitlab.com/u/gkopylov)) 

### Detailed changes 
0. [new][S] Bump project version 

### Issues 
* Bump VERSION ([#2](https://gitlab.com/technoelegance/gitlab_test_for_kanban/issues/2))

## v0.0.1 (13.07.2016 00:23) 

### Changes 
* Add VERSION file ([#1](https://gitlab.com/technoelegance/gitlab_test_for_kanban/merge_requests/1) by [@gkopylov](https://gitlab.com/u/gkopylov)) 

### Detailed changes 
0. [new][S] Add file with project version 

### Issues 

